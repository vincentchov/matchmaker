Matchmaker
==============================================

This Python package is for taking a Google Forms questionnaire's output
(CSV format).  It compares peoples' answers to other people who are in a 20-mile
radius of each other and lists the number of questions each person has in common.

Setup from source code (Git)
-------------------------------
1. Clone the repo.  ``git clone https://bitbucket.org/vincentchov/matchmaker.git``
2. Install `Python 3.x <https://www.python.org/downloads>`_ with pip.
3. Create and activate a virtual environment.
4. Install dependencies: ``pip install -r requirements.txt``.
5. Profit!

Usage
-----
1. ``cd`` into the matchmaker directory.
2. Copy your Google Forms CSV file to this folder.
3. ``python matchmaker.py -csv <csv_filename_here>``
4. Get your results printed as a JSON.

More explicit instructions
--------------------------
The following instructions walk you through installing and running a Python program, for people who have never installed or run Python (or any other command line program for that matter).

1) First you will want to install `Python 3.x <https://www.python.org/downloads>`_, adding it to your PATH environment variable, as shown in the attached screenshot.  It should be the same in Mac OS.  In Linux, you just install it and it should already be in your PATH.

2) Install `Git <https://git-scm.com/downloads>`_ and leave everything by default except for the PATH.  In that section, choose to run Git from the Windows Command Prompt instead of Git Bash if you're installing for Windows.  It should automatically work in Mac and Linux, so you don't need to specify whether to use the built-in Windows CMD vs a Bash Terminal.

3) Open up command prompt (Terminal in Mac and Linux) and go to your downloads folder by typing ``cd Downloads`` and hit Enter.

4) Get a copy (aka "clone") of my project using Git: ``git clone https://bitbucket.org/vincentchov/matchmaker.git``.  There should now be a folder called "matchmaker" in your Downloads folder, with another folder inside it, also called "matchmaker".

5) Go into the first level of the project folder by typing ``cd matchmaker`` and hitting Enter.

6) Inside this folder, there should be a requirements.txt file listing out all of the dependencies you should have installed to run the program.  Run ``pip install -r requirements.txt`` to automatically install all of them.

7) Now that you have all of the dependencies, you're ready to go into the inner matchmaker folder, which contains the actual code.  ``cd matchmaker``

8) Put a copy of your Google Forms' CSV file into that folder and give it a name that's easy for you to type out, like "answers.csv" or something similar.

9) To run the program, you type the command that runs Python code from a file, which is of the form ``python your_filename.py``, but add the ``-csv`` option and supply the name of your file.  For example, ``python matchmaker.py -csv answers.csv``.

10) The program will output all of the results in JSON format, where each user is identified by their email address.  For example, if John Doe (john.doe@gmail.com) is matched with Jane Doe (jane.doe@gmail.com) and Jane answered 12 questions the same as him, but Ro Khanna (ro.khanna@gmail.com) only answered 5 questions the same as John, then John's entry will look like  

.. code-block:: javascript  

    "john.doe@gmail.com": {
        "jane.doe@gmail.com": 12,  
        "ro.khanna@gmail.com": 5
    }
