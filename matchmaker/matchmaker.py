'''matchmaker
Usage:
  matchmaker -csv <csv_name>
  matchmaker -h | --help
  matchmaker --version
Options:
  -h --help     Show this screen.
  --version     Show version.
'''
import zipcode
import pandas as pd
import json
import sys
from docopt import docopt
from collections import OrderedDict


__version__ = "0.1.0"
__author__ = "Vincent Chov"
__license__ = "MIT"


if __name__ == "__main__":
    """ Main entry point for the matchmaker CLI. """
    try:
        args = docopt(__doc__, version=__version__)
        filename = str(args['<csv_name>'])
    except ValueError as e:
        exit("You must supply csv filename.")
else:
    filename = input("Enter a CSV filename: ")


df = pd.read_csv(filename)
for col in df.columns.values:
    if "zipcode" in str(col).lower().replace(" ", ""):
        col_to_rename = col
        df = df.rename(index=str, columns={col_to_rename: "zipcode"})
        df["zipcode"] = df["zipcode"].apply(str)
        break


def match_for_one_zipcode(home, distance):
    """
        Given a zipcode, return all zipcodes that are within a given
        radius (miles) and also belong to someone in the survey.
    """
    nearby = zipcode.isinradius((home.lat, home.lon), distance)
    matches_for_zipcode = [
        x.zip for x in nearby
        if df["zipcode"].str.contains(x.zip).any() and x.zip != home.zip
    ]
    matches_for_zipcode.sort()
    return matches_for_zipcode


def match_all_zipcodes(distance):
    """
        Returns dictionary of all zipcodes that should be matched
        with each one.
    """
    matches = {}

    for zipcode_str in df["zipcode"]:
        home_zipcode = zipcode.isequal(zipcode_str)
        results = match_for_one_zipcode(home_zipcode, distance)
        if results:
            matches[zipcode_str] = results

    return matches


def get_matching_answers(first_person_row, second_person_row):
    # Given two people, return the columns with exact matches
    matching_answers = []
    for column in first_person_row.keys():
        if first_person_row[column] == second_person_row[column]:
            matching_answers.append(column)
    return matching_answers


def score_adjacent_people(person_row, adjacent_zipcodes):
    adjacent_people = pd.DataFrame(columns=df.columns)
    for adj_zip in adjacent_zipcodes:
        adjacent_people = adjacent_people.append(df[df["zipcode"] == adj_zip])

    person_email = person_row["Email"]
    scores = {}
    for i, adj_person in adjacent_people.iterrows():
        scores[adj_person["Email"]] = (
            len(get_matching_answers(person_row, adj_person))
        )
    scores = OrderedDict(sorted(scores.items(),
                         key=lambda t: t[1],
                         reverse=True))
    return (person_email, scores)


if __name__ == "__main__":
    results = {}
    matches = match_all_zipcodes(20)
    for zipcode_str in matches:
        people = df[df["zipcode"] == zipcode_str]
        for i, person_row in people.iterrows():
            adjacent_zipcodes = matches[zipcode_str]
            email, scores = score_adjacent_people(person_row, adjacent_zipcodes)
            results[email] = scores
    print(json.dumps(results, indent=4))
